# Install FFMPEG on Mac OS X (Apple Silicon)

brew install pkg-config
brew install libass-devel
sudo mkdir -p /opt/local/src
cd /opt/local/src
sudo git clone https://git.ffmpeg.org/ffmpeg.git ffmpeg
cd ffmpeg
sudo ./configure --prefix=/opt/local
sudo make
sudo make install
/opt/local/bin/ffmpeg -version